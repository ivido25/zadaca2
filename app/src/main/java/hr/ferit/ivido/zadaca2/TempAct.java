package hr.ferit.ivido.zadaca2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class TempAct extends Activity implements View.OnClickListener {

    public static final String KEY_INPUT_DATA = "key_input";
    public static final String KEY_ENTERED_DATA = "key_entered";
    public static final String KEY_SPINUP_NAME = "key_spinup";
    public static final String KEY_SPINDOWN_NAME = "key_spindown";
    public static final String KEY_ACTIVITY_NAME= "key_activity";
    private static final String ACTIVITY_NAME="TempAct";

    Spinner spinUp, spinDown;
    EditText etInput;
    Button bConvert;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp);
        this.etInput = (EditText) findViewById(R.id.etInput);
        this.bConvert = (Button) findViewById(R.id.bConvert);
        this.spinDown = (Spinner) findViewById(R.id.spinDown);
        this.spinUp = (Spinner) findViewById(R.id.spinUp);


        bConvert.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        String sSpinDown=spinDown.getSelectedItem().toString();
        String sSpinUp=spinUp.getSelectedItem().toString();



        double enteredVal;
        enteredVal = Double.parseDouble(etInput.getText().toString());
        double input = convertValue(sSpinDown,sSpinUp,enteredVal);
        Intent explicitIntent = new Intent(getApplicationContext(), ResultActivity.class);



        explicitIntent.putExtra(KEY_INPUT_DATA, input);
        explicitIntent.putExtra(KEY_ENTERED_DATA, enteredVal);
        explicitIntent.putExtra(KEY_SPINUP_NAME, sSpinUp );
        explicitIntent.putExtra(KEY_SPINDOWN_NAME, sSpinDown);
        explicitIntent.putExtra(KEY_ACTIVITY_NAME, ACTIVITY_NAME);


        this.startActivity(explicitIntent);

    }

    private double convertValue(String sSpinDown, String sSpinUp, double enteredVal) {
        double coef=1;

        switch (sSpinUp)
        {
            case("Celsius"):{
                switch (sSpinDown){
                    case("Celsius"):    coef=enteredVal;
                        break;
                    case("Fahrenheit"): coef=enteredVal*1.8+32;
                        break;
                    case("Kelvin"):     coef=enteredVal+273.15;
                        break;
                }}
            break;
            case("Fahrenheit"):{
                switch (sSpinDown){
                    case("Celsius"):    coef=(enteredVal-32)/1.8;
                        break;
                    case("Fahrenheit"): coef=enteredVal;
                        break;
                    case("Kelvin"):     coef=(enteredVal-32)/1.8+273.15;
                        break;
                }}
            break;
            case("Kelvin"):{
                switch (sSpinDown){
                    case("Celsius"):    coef=enteredVal-273.15;
                        break;
                    case("Fahrenheit"): coef=(enteredVal-273.15)*1.8+32;
                        break;
                    case("Kelvin"):     coef=1;
                        break;

                }}
            break;
        }
        return coef;
    }
}
