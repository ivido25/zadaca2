package hr.ferit.ivido.zadaca2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends Activity implements View.OnClickListener {

    ImageView ivTemp, ivKmToMiles, ivVolume, ivFeetToMeter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initUi();
    }

    private void initUi() {
        this.ivFeetToMeter =  (ImageView) findViewById(R.id.ivFeetToMeter);
        this.ivKmToMiles = (ImageView) findViewById(R.id.ivKmToMiles);
        this.ivTemp = (ImageView) findViewById(R.id.ivTemp);
        this.ivVolume = (ImageView) findViewById(R.id.ivVolume);

        this.ivFeetToMeter.setOnClickListener(this);
        this.ivKmToMiles.setOnClickListener(this);
        this.ivTemp.setOnClickListener(this);
        this.ivVolume.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        Intent explicitIntent = new Intent();
    switch(v.getId()){
        case(R.id.ivFeetToMeter):
            explicitIntent.setClass(getApplicationContext(), DistanceAct.class);
            this.startActivity(explicitIntent);
            break;
        case(R.id.ivKmToMiles):
            explicitIntent.setClass(getApplicationContext(), SpeedAct.class);
            this.startActivity(explicitIntent);
            break;
        case(R.id.ivTemp):
            explicitIntent.setClass(getApplicationContext(), TempAct.class);
            this.startActivity(explicitIntent);
            break;
        case(R.id.ivVolume):
            explicitIntent.setClass(getApplicationContext(), VolumeAct.class);
            this.startActivity(explicitIntent);
            break;
    }
    }
}
