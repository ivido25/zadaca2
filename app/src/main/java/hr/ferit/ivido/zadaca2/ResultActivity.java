package hr.ferit.ivido.zadaca2;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.TextView;

import java.util.Locale;

import static android.R.id.input;

public class ResultActivity extends Activity {

    TextView tvFrom, tvTo, tvFromVal, tvToVal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        this.tvFrom = (TextView) findViewById(R.id.tvFrom);
        this.tvTo = (TextView) findViewById(R.id.tvTo);
        this.tvFromVal = (TextView) findViewById(R.id.tvFromVal);
        this.tvToVal = (TextView) findViewById(R.id.tvToVal);




        Intent startingIntent = this.getIntent();
        //String actName=startingIntent.getStringExtra("key_activity");

        double key =0;
        String sCon,sIn;

        double converted = startingIntent.getDoubleExtra("key_input",key);
        double input = startingIntent.getDoubleExtra("key_entered",key);

        sCon=String.format(Locale.GERMANY,"%.3f",converted);
        sIn=String.format(Locale.GERMANY,"%.3f",input);

        tvToVal.setText(sCon);
        tvFromVal.setText(sIn);
        tvFrom.setText(startingIntent.getStringExtra("key_spinup"));
        tvTo.setText(startingIntent.getStringExtra("key_spindown"));

       // displayContent(actName,startingIntent);



    }
        //Proizvod ne razumjevanja kako intenti prenose vrijednosti debug pomogao
    private void displayContent(String actName, Intent startingIntent) {
        double key =0;
        String sCon,sIn;
        switch (actName){
            case("SpeedAct"): {
                double converted = startingIntent.getDoubleExtra(SpeedAct.KEY_INPUT_DATA,key);
                double input = startingIntent.getDoubleExtra(SpeedAct.KEY_ENTERED_DATA,key);

                sCon=String.format(Locale.GERMANY,"%.3f",converted);
                sIn=String.format(Locale.GERMANY,"%.3f",input);

                tvToVal.setText(sCon);
                tvFromVal.setText(sIn);
                tvFrom.setText(startingIntent.getStringExtra(SpeedAct.KEY_SPINUP_NAME));
                tvTo.setText(startingIntent.getStringExtra(SpeedAct.KEY_SPINDOWN_NAME));
            }
            break;
            case("DistanceAct"): {
                double converted = startingIntent.getDoubleExtra(DistanceAct.KEY_INPUT_DATA, key);
                double input = startingIntent.getDoubleExtra(DistanceAct.KEY_ENTERED_DATA, key);

                sCon = String.format(Locale.GERMANY, "%.3f", converted);
                sIn = String.format(Locale.GERMANY, "%.3f", input);

                tvToVal.setText(sCon);
                tvFromVal.setText(sIn);
                tvFrom.setText(startingIntent.getStringExtra(DistanceAct.KEY_SPINUP_NAME));
                tvTo.setText(startingIntent.getStringExtra(DistanceAct.KEY_SPINDOWN_NAME));
            }
            break;
            case("TempAct"): {
                double converted = startingIntent.getDoubleExtra(TempAct.KEY_INPUT_DATA, key);
                double input = startingIntent.getDoubleExtra(TempAct.KEY_ENTERED_DATA, key);

                sCon = String.format(Locale.GERMANY, "%.3f", converted);
                sIn = String.format(Locale.GERMANY, "%.3f", input);

                tvToVal.setText(sCon);
                tvFromVal.setText(sIn);
                tvFrom.setText(startingIntent.getStringExtra(TempAct.KEY_SPINUP_NAME));
                tvTo.setText(startingIntent.getStringExtra(TempAct.KEY_SPINDOWN_NAME));
            }
            break;
            case("VolumeAct"):{
                double converted = startingIntent.getDoubleExtra(VolumeAct.KEY_INPUT_DATA,key);
                double input = startingIntent.getDoubleExtra(VolumeAct.KEY_ENTERED_DATA,key);

                sCon=String.format(Locale.GERMANY,"%.3f",converted);
                sIn=String.format(Locale.GERMANY,"%.3f",input);

                tvToVal.setText(sCon);
                tvFromVal.setText(sIn);
                tvFrom.setText(startingIntent.getStringExtra(VolumeAct.KEY_SPINUP_NAME));
                tvTo.setText(startingIntent.getStringExtra(VolumeAct.KEY_SPINDOWN_NAME));
            }
            break;
        }
    }
}
