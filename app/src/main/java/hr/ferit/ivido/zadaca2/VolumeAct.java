package hr.ferit.ivido.zadaca2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class VolumeAct extends Activity implements View.OnClickListener {
    public static final String KEY_INPUT_DATA = "key_input";
    public static final String KEY_ENTERED_DATA = "key_entered";
    public static final String KEY_SPINUP_NAME = "key_spinup";
    public static final String KEY_SPINDOWN_NAME = "key_spindown";
    public static final String KEY_ACTIVITY_NAME= "key_activity";
    private static final String ACTIVITY_NAME="VolumeAct";

    Spinner spinUp, spinDown;
    EditText etInput;
    Button bConvert;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volume);
        this.etInput = (EditText) findViewById(R.id.etInput);
        this.bConvert = (Button) findViewById(R.id.bConvert);
        this.spinDown = (Spinner) findViewById(R.id.spinDown);
        this.spinUp = (Spinner) findViewById(R.id.spinUp);


        bConvert.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        String sSpinDown=spinDown.getSelectedItem().toString();
        String sSpinUp=spinUp.getSelectedItem().toString();
        double coef = getCoeficient(sSpinDown,sSpinUp);


        double enteredVal;
        enteredVal = Double.parseDouble(etInput.getText().toString());
        double input = enteredVal*coef;
        Intent explicitIntent = new Intent(getApplicationContext(), ResultActivity.class);



        explicitIntent.putExtra(KEY_INPUT_DATA, input);
        explicitIntent.putExtra(KEY_ENTERED_DATA, enteredVal);
        explicitIntent.putExtra(KEY_SPINUP_NAME, sSpinUp );
        explicitIntent.putExtra(KEY_SPINDOWN_NAME, sSpinDown);
        explicitIntent.putExtra(KEY_ACTIVITY_NAME, ACTIVITY_NAME);


        this.startActivity(explicitIntent);

    }

    private double getCoeficient(String sSpinDown, String sSpinUp) {
        double coef=1;

        switch (sSpinUp)
        {
            case("Liter"):{
                switch (sSpinDown){
                    case("Liter"):              coef=1;
                        break;
                    case("Ounce(US)"):          coef=33.814;
                        break;
                    case("Cubic Centimeter"):   coef=1000;
                        break;
                    case("Gallon(US)"):         coef=0.264172;
                        break;
                }}
            break;
            case("Ounce(US)"):{
                switch (sSpinDown){
                    case("Liter"):              coef=0.0295735;
                        break;
                    case("Ounce(US)"):          coef=1;
                        break;
                    case("Cubic Centimeter"):   coef=29.5735;
                        break;
                    case("Gallon(US)"):         coef=0.0078125;
                        break;
                }}
            break;
            case("Cubic Centimeter"):{
                switch (sSpinDown){
                    case("Liter"):              coef=0.001;
                        break;
                    case("Ounce(US)"):          coef=0.033814;
                        break;
                    case("Cubic Centimeter"):   coef=1;
                        break;
                    case("Gallon(US)"):         coef=0.000264172;
                        break;

                }}
            break;
            case("Gallon(US)"):{
                switch (sSpinDown){
                    case("Liter"):              coef=3.78541;
                        break;
                    case("Ounce(US)"):          coef=128;
                        break;
                    case("Cubic Centimeter"):   coef=3785.41;
                        break;
                    case("Gallon(US)"):         coef=1;
                        break;
                }}
            break;

        }
        return coef;
    }
}
