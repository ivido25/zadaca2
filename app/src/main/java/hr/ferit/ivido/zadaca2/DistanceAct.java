package hr.ferit.ivido.zadaca2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class DistanceAct extends Activity implements View.OnClickListener {

    public static final String KEY_INPUT_DATA = "key_input";
    public static final String KEY_ENTERED_DATA = "key_entered";
    public static final String KEY_SPINUP_NAME = "key_spinup";
    public static final String KEY_SPINDOWN_NAME = "key_spindown";
    public static final String KEY_ACTIVITY_NAME= "key_activity";
    private static final String ACTIVITY_NAME="DistanceAct";

    Spinner spinUp, spinDown;
    EditText etInput;
    Button bConvert;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distance);
        this.etInput = (EditText) findViewById(R.id.etInput);
        this.bConvert = (Button) findViewById(R.id.bConvert);
        this.spinDown = (Spinner) findViewById(R.id.spinDown);
        this.spinUp = (Spinner) findViewById(R.id.spinUp);


        bConvert.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        String sSpinDown=spinDown.getSelectedItem().toString();
        String sSpinUp=spinUp.getSelectedItem().toString();
        double coef = getCoeficient(sSpinDown,sSpinUp);


        double enteredVal;
        enteredVal = Double.parseDouble(etInput.getText().toString());
        double input = enteredVal*coef;
        Intent explicitIntent = new Intent(getApplicationContext(), ResultActivity.class);



        explicitIntent.putExtra(KEY_INPUT_DATA, input);
        explicitIntent.putExtra(KEY_ENTERED_DATA, enteredVal);
        explicitIntent.putExtra(KEY_SPINUP_NAME, sSpinUp );
        explicitIntent.putExtra(KEY_SPINDOWN_NAME, sSpinDown);
        explicitIntent.putExtra(KEY_SPINDOWN_NAME, sSpinDown);
        explicitIntent.putExtra(KEY_ACTIVITY_NAME, ACTIVITY_NAME);

        this.startActivity(explicitIntent);

    }

    private double getCoeficient(String sSpinDown, String sSpinUp) {
        double coef=1;
        switch (sSpinUp)
        {
            case("Meter"):{
                switch (sSpinDown){
                    case("Meter"):      coef=1;
                        break;
                    case("Kilometer"):  coef=0.001;
                        break;
                    case("Mile"):       coef=0.000621371;
                        break;
                    case("Inch"):       coef=39.3701;
                        break;
                    case("Yard"):       coef=1.09361;
                        break;
                    case("Centimeter"): coef=100;
                        break;
                    case("Feet"):       coef=3.28084;
                        break;
                }}
            break;
            case("Kilometer"):{
                switch (sSpinDown){
                    case("Meter"):      coef=1000;
                        break;
                    case("Kilometer"):  coef=1;
                        break;
                    case("Mile"):       coef=0.621371;
                        break;
                    case("Inch"):       coef=39370.1;
                        break;
                    case("Yard"):       coef=1093.61;
                        break;
                    case("Centimeter"): coef=100000;
                        break;
                    case("Feet"):       coef=3280.84;
                        break;
                }}
            break;
            case("Mile"):{
                switch (sSpinDown){
                    case("Meter"):      coef=1609.34;
                        break;
                    case("Kilometer"):  coef=1.60934;
                        break;
                    case("Mile"):       coef=1;
                        break;
                    case("Inch"):       coef=63360;
                        break;
                    case("Yard"):       coef=1760;
                        break;
                    case("Centimeter"): coef=160934;
                        break;
                    case("Feet"):       coef=5280;
                        break;
                }}
            break;
            case("Inch"):{
                switch (sSpinDown){
                    case("Meter"):      coef=0.0254;
                        break;
                    case("Kilometer"):  coef=0.0000254;
                        break;
                    case("Mile"):       coef=1.5783e-5;
                        break;
                    case("Inch"):       coef=1;
                        break;
                    case("Yard"):       coef=0.0277778;
                        break;
                    case("Centimeter"): coef=2.540002032;
                        break;
                    case("Feet"):       coef=0.0833334;
                        break;
                }}
            break;
            case("Yard"):{
                switch (sSpinDown){
                    case("Meter"):      coef=0.9144;
                        break;
                    case("Kilometer"):  coef=0.0009144;
                        break;
                    case("Mile"):       coef=0.000568182;
                        break;
                    case("Inch"):       coef=36;
                        break;
                    case("Yard"):       coef=1;
                        break;
                    case("Centimeter"): coef=91.44;
                        break;
                    case("Feet"):       coef=3;
                        break;
                }}
            break;
            case("Centimeter"):{
                switch (sSpinDown){
                    case("Meter"):      coef=0.01;
                        break;
                    case("Kilometer"):  coef=0.00001;
                        break;
                    case("Mile"):       coef=6.2137e-6;
                        break;
                    case("Inch"):       coef=0.393701;
                        break;
                    case("Yard"):       coef=0.0109361;
                        break;
                    case("Centimeter"): coef=1;
                        break;
                    case("Feet"):       coef=0.0328084;
                        break;
                }}
            break;
            case("Feet"):{
                switch (sSpinDown){
                    case("Meter"):      coef=0.3048;
                        break;
                    case("Kilometer"):  coef=0.0003048;
                        break;
                    case("Mile"):       coef=0.000189394;
                        break;
                    case("Inch"):       coef=12;
                        break;
                    case("Yard"):       coef=0.333333;
                        break;
                    case("Centimeter"): coef=30.4;
                        break;
                    case("Feet"):       coef=1;
                        break;
                }}
            break;

        }
        return coef;
    }
}
